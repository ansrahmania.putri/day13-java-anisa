package com.codeflex.springboot.controller;

import com.codeflex.springboot.entity.Product;
import com.codeflex.springboot.entity.ProductDet;
import com.codeflex.springboot.repo.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class RestApiProductController {

    public static final Logger logger = LoggerFactory.getLogger(RestApiProductController.class);

    @Autowired
    private ProductRepo productRepo;

    // -------------------Create a Product-------------------------------------------
    // Save to DB - Insert to Database
    @RequestMapping(value = "/productrepo/", method = RequestMethod.POST, produces="application/json")
    public ResponseEntity<?> createProductNew(@RequestBody Product product) throws SQLException, ClassNotFoundException {

        Product createProduct = new Product();
        createProduct.setName(product.getName());
        createProduct.setCategoryId(product.getCategoryId());
        createProduct.setPrice(product.getPrice());

        List<ProductDet> productDets = product.getProductDet();
        for (ProductDet prodDetObj: productDets) {
            ProductDet productDet = new ProductDet();
            productDet.setKet(prodDetObj.getKet());
            createProduct.addProdDet(productDet);
        }
        productRepo.save(createProduct);
        return new ResponseEntity<>(createProduct, HttpStatus.CREATED);
    }

    // -------------------Update a Product-------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateProduct(@PathVariable("id") int id, @RequestBody Product product) {

        Product currentProduct = productRepo.findById(id).get();

        currentProduct.setName(product.getName());
        currentProduct.setCategoryId(product.getCategoryId());
        currentProduct.setPrice(product.getPrice());

        List<ProductDet> productDets = currentProduct.getProductDet();
        int i =0;
        for (ProductDet elProdDet : productDets) {
            elProdDet.setKet(product.getProductDet().get(i).getKet());
            i++;
        }

        currentProduct.setProductDet(productDets);

        productRepo.save(currentProduct);
        return new ResponseEntity<>(currentProduct, HttpStatus.OK);
    }

    // ------------------- Delete a Product-----------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteProduct(@PathVariable("id") int id) {
        logger.info("Fetching & Deleting Product with id {}", id);

        Product product = productRepo.findById(id).get();

        productRepo.delete(product);
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }

    // -------------------Retrieve All Products--------------------------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.GET, produces="application/json")
    public ResponseEntity<List<Product>> listAllProducts() {

        List<Product> products = productRepo.findAll();

        return new ResponseEntity<>(products, HttpStatus.OK);
    }

    // -------------------Retrieve Single Product------------------------------------------

    @RequestMapping(value = "/productrepo/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getProduct(@PathVariable("id") int id) {
        logger.info("Fetching Product with id {}", id);

        Product product = productRepo.findById(id).get();

        return new ResponseEntity<>(product, HttpStatus.OK);
    }


    // ------------------- Delete All Products-----------------------------

    @RequestMapping(value = "/productrepo/", method = RequestMethod.DELETE)
    public ResponseEntity<Product> deleteAllProducts() {
        logger.info("Deleting All Products");

        productRepo.deleteAll();
        return new ResponseEntity<Product>(HttpStatus.NO_CONTENT);
    }





}